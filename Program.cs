﻿using System;
using System.Collections.Generic;

namespace hamming
{
    class Program
    {
        static void Main(string[] args)
        {
            Receipt receipt = new Receipt("Пятёрочка");
            receipt.Add(new Position("Колбаса докторская", count: 2, price: 189.99));
            receipt.Add(new Position("Молоко в деревне", count: 1, price: 54.99));
            receipt.Add(new Position("Сыр Российский", count: 1, price: 256.99));
            Console.WriteLine($"Итого к оплате: {receipt.Sum}");
            Console.WriteLine($"Итого к оплате: {receipt.Sum}");

        }

    }
    class Receipt
    {
        private string nameStore;
        private List<Position> positions;

        public Receipt(string name)
        {
            positions = new List<Position>();
            nameStore = name;
        }
        
        public void Add(Position position)
        {
            positions.Add(position);
        }

        public double Sum
        {
            get
            {
                double counter = 0;
                foreach (var line in positions)
                {
                    counter = counter + line.TotalAmount;
                }
                return counter;
            }
            
        }

        

    }
    class Position
    {
        private double priceProduct;
        private int countProduct;
        private string nameProduct;
        

        public Position(string name, int count, double price)
        {
            nameProduct = name;
            countProduct = count;
            priceProduct = price;

            
        }
          
        public double TotalAmount
        {
            get
            {
                return (priceProduct * countProduct);
            }
            
        }
    }
}
